import re


#filename = 'kepco-BOP502M.db'
filename = 'kepco.db'

match = ['HIHI','LOLO','HIGH','LOW','HHSV','LLSV','HSV','LSV','HYST']
#pattern = re.compile(r"record\w*pe$", re.IGNORECASE)

record = []
all_records = []

with open(filename, 'rt') as myfile:
    for line in myfile:
       line = line.partition('#')[0] # remove comments 

       if re.match(r'^\s*$', line):  # remove empty line
          continue

       line = re.sub(' ','',line)    # remove space

       if re.search('record',line):  # search for records
          name = re.split(',',line)
          named = re.sub('["{]','',name[1].strip('\n'))[:-1]  # remove chars "{ 
          record.append(named)
          continue

       if re.search('DESC',line):  # search for DESC field
          desc = re.split(',',line)
          desc = re.sub('["{]','',desc[1].strip('\n'))[:-1]  # remove charas "{ 
          record.append(desc)
          continue

       if any(word in line for word in match): # find all match words
          fields = re.split("field",line) 
          fields = list(filter(None,fields))   # remove empty string at the beginning
          for f in fields:
             field = re.sub('["()]','',f).strip('\n')  # remove chars "() 
             record.append(re.split(',',field))
       
       if re.search('}',line):
          print(record)
          print('--------')
          all_records.append(record)
          record.clear()

print(all_records[1])
